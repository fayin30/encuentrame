﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Encuentrame.Business.Data
{
  public abstract class BaseData
  {
    public SqlDataReader GetDataReader(string cmdText, SqlParameter[] parameters)
    {
      var conn = ConfigurationManager.ConnectionStrings["connectionStringEncuentrameDB"].ToString();
      using (var sqlconn = new SqlConnection(conn))
      {
        sqlconn.Open();
        var command = new SqlCommand(cmdText, sqlconn);
        command.CommandType = CommandType.StoredProcedure;
        command.Parameters.AddRange(parameters);
        var datareader = command.ExecuteReader();
        sqlconn.Close();
        return datareader;
      }
    }

    public int Insert(string cmdText, SqlParameter[] parameters, string parameterId = null)
    {
      var conn = ConfigurationManager.ConnectionStrings["connectionStringEncuentrameDB"].ToString();
      using (var sqlconn = new SqlConnection(conn))
      {
        sqlconn.Open();
        var command = new SqlCommand(cmdText, sqlconn);
        command.CommandType = CommandType.StoredProcedure;
        command.Parameters.AddRange(parameters);        
        command.ExecuteNonQuery();
        var id = 0;
        if (!string.IsNullOrWhiteSpace(parameterId))
        {
          id = Convert.ToInt32(command.Parameters[parameterId].Value);
        }
        sqlconn.Close();
        return id;
      }
    }

    public bool Update(string cmdText, SqlParameter[] parameters)
    {
      var conn = ConfigurationManager.ConnectionStrings["connectionStringEncuentrameDB"].ToString();
      using (var sqlconn = new SqlConnection(conn))
      {
        sqlconn.Open();
        var command = new SqlCommand(cmdText, sqlconn);
        command.CommandType = CommandType.StoredProcedure;
        command.Parameters.AddRange(parameters);        
        var result = command.ExecuteNonQuery();
        var exito = false;
        if (result > 0)
          exito = true;
        sqlconn.Close();
        return exito;
      }
    }

    public bool Delete(string cmdText, SqlParameter[] parameters)
    {
      var conn = ConfigurationManager.ConnectionStrings["connectionStringEncuentrameDB"].ToString();
      using (var sqlconn = new SqlConnection(conn))
      {
        sqlconn.Open();
        var command = new SqlCommand(cmdText, sqlconn);
        command.CommandType = CommandType.StoredProcedure;
        command.Parameters.AddRange(parameters);
        var result = command.ExecuteNonQuery();
        var exito = false;
        if (result > 0)
          exito = true;
        sqlconn.Close();
        return exito;
      }
    }
  }
}
