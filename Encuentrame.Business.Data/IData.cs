﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encuentrame.Business.Data
{
  public interface IData<T>
  {
    T Registrar(T t);
    bool Actualizar(T t);
    bool Eliminar(T t);
    T ObtenerPorId(int id);
  }
}
