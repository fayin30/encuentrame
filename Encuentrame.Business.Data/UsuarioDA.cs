﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Encuentrame.Business.Entity;
using System.Data.SqlClient;
using System.Data;

namespace Encuentrame.Business.Data
{
    public class UsuarioDA : BaseData, IData<Security>
    {
        public bool Actualizar(Security t)
        {
            throw new NotImplementedException();
        }

        public bool Eliminar(Security t)
        {
            throw new NotImplementedException();
        }

        public Security ObtenerPorId(int id)
        {
            throw new NotImplementedException();
        }

        public Security Registrar(Security t)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@IdUsuario", SqlDbType.Int));
            parameters[0].Direction = ParameterDirection.Output;
            parameters.Add(new SqlParameter("@IdPersona", SqlDbType.VarChar));
            parameters[1].Value = t.Persona.Id;
            parameters.Add(new SqlParameter("@Login", SqlDbType.VarChar));
            parameters[2].Value = t.Login;
            parameters.Add(new SqlParameter("@Password", SqlDbType.NVarChar));
            parameters[3].Value = t.Password;

            var idUsuario = base.Insert("uspUsuarioAdd", parameters.ToArray(), "@IdUsuario");
            t.IdUser = idUsuario;

            return t;
        }
    }
}
