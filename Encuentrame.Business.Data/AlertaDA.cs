﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Encuentrame.Business.Entity;

namespace Encuentrame.Business.Data
{
  public class AlertaDA : BaseData, IData<Alerta>
  {
    public bool Actualizar(Alerta t)
    {
      throw new NotImplementedException();
    }

    public bool Eliminar(Alerta t)
    {
      throw new NotImplementedException();
    }

    public Alerta ObtenerPorId(int id)
    {
      Alerta alerta = null;
      var parameters = new List<SqlParameter>();
      parameters.Add(new SqlParameter("@IdAlerta", SqlDbType.Int));
      using (var dr = base.GetDataReader("uspAlertaGetxid", parameters.ToArray()))
      {
        while (dr.Read())
        {
          alerta = new Alerta();
          alerta.IdAlerta = dr.GetInt32(dr.GetOrdinal("IdAlerta"));
          alerta.PersonaAlerta = new Persona() { IdPersona = dr.GetInt32(dr.GetOrdinal("IdPersonaAlerta")) };
          alerta.Busqueda = new Busqueda() { IdBusqueda = dr.GetInt32(dr.GetOrdinal("IdBusqueda")) };
          alerta.FechaAlerta = dr.GetDateTime(dr.GetOrdinal("FechaAlerta"));
          alerta.Referencia = dr.GetString(dr.GetOrdinal("Referencia"));
          alerta.Detalle = dr.GetString(dr.GetOrdinal("Detalle"));
          alerta.Direccion = dr.GetString(dr.GetOrdinal("Direccion"));
          alerta.Pais = dr.GetString(dr.GetOrdinal("Pais"));
          alerta.Region = dr.GetString(dr.GetOrdinal("Region"));
          alerta.Ciudad = dr.GetString(dr.GetOrdinal("Ciudad"));
        }
      }

      return alerta;
    }

    public Alerta Registrar(Alerta t)
    {
      Alerta alerta = null;
      var parameters = new List<SqlParameter>();
      var par = new SqlParameter("@IdPersonaAlerta", SqlDbType.Int);
      par.Value = t.PersonaAlerta.IdPersona;
      parameters.Add(par);
      par = new SqlParameter("@IdBusqueda", SqlDbType.Int);
      par.Value = t.Busqueda.IdBusqueda;
      parameters.Add(par);
      par = new SqlParameter("@Referencia", SqlDbType.VarChar);
      par.Value = t.Referencia;
      parameters.Add(par);
      par = new SqlParameter("@Detalle", SqlDbType.VarChar);
      par.Value = t.Detalle;
      parameters.Add(par);
      par = new SqlParameter("@Direccion", SqlDbType.VarChar);
      par.Value = t.Direccion;
      parameters.Add(par);
      par = new SqlParameter("@Referencia", SqlDbType.VarChar);
      par.Value = t.Referencia;
      parameters.Add(par);
      par = new SqlParameter("@Pais", SqlDbType.VarChar);
      par.Value = t.Pais;
      parameters.Add(par);
      par = new SqlParameter("@Region", SqlDbType.VarChar);
      par.Value = t.Region;
      parameters.Add(par);
      par = new SqlParameter("@Ciudad", SqlDbType.VarChar);
      par.Value = t.Ciudad;
      parameters.Add(par);

      base.Insert("uspAlertaAdd", parameters.ToArray());
      
      return alerta;
    }
  }
}
