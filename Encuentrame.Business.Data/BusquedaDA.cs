﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Encuentrame.Business.Entity;
using System.Data.SqlClient;
using System.Data;

namespace Encuentrame.Business.Data
{
    public class BusquedaDA : BaseData, IData<Busqueda>
    {
        public bool Actualizar(Busqueda t)
        {
            throw new NotImplementedException();
        }

        public bool Eliminar(Busqueda t)
        {
            throw new NotImplementedException();
        }

        public Busqueda ObtenerPorId(int id)
        {
            throw new NotImplementedException();
        }

        public List<Busqueda> ObtenerPorFecha(DateTime fechaIni, DateTime fechaFin)
        {
            List<Busqueda> listaBusqueda = new List<Busqueda>();
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@FechaPerdidaIni", SqlDbType.DateTime));
            parameters[0].Value = fechaIni;
            parameters.Add(new SqlParameter("@FechaPerdidaFin", SqlDbType.Date));
            parameters[0].Value = fechaFin;
            using (var dr = base.GetDataReader("uspBusquedaGetXFecha", parameters.ToArray()))
            {
                while (dr.Read())
                {
                    listaBusqueda.Add(new Busqueda()
                    {
                        Ciudad = dr.GetString(dr.GetOrdinal("Ciudad")),
                        Detalles = dr.GetString(dr.GetOrdinal("Detalles")),
                        Encontrado = dr.GetBoolean(dr.GetOrdinal("Encontrado")),
                        FechaEncuentro = dr.GetDateTime(dr.GetOrdinal("FechaEncuentro")),
                        FechaPerdida = dr.GetDateTime(dr.GetOrdinal("FechaPerdida")),
                        IdBusqueda = dr.GetInt32(dr.GetOrdinal("IdBusqueda")),
                        LugarPerdida = dr.GetString(dr.GetOrdinal("LugarPerdida")),
                        Pais = dr.GetString(dr.GetOrdinal("Pais")),
                        Referencia = dr.GetString(dr.GetOrdinal("Referencia")),
                        Region = dr.GetString(dr.GetOrdinal("Region")),
                        PersonaResponsable = new Persona() { Id = dr.GetInt32(dr.GetOrdinal("IdPersonaResponsable")) },
                        PersonaPerdida = new Persona() { Id = dr.GetInt32(dr.GetOrdinal("IdPersonaPerdida")) },
                    });
                }
            }

            return listaBusqueda;
        }

        public Busqueda Registrar(Busqueda t)
        {
            Busqueda persona = null;
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@IdPersonaResponsable", SqlDbType.Int));
            parameters[0].Value = t.PersonaResponsable.Id;
            parameters.Add(new SqlParameter("@IdPersonaPerdida", SqlDbType.VarChar));
            parameters[1].Value = t.PersonaPerdida.Id;
            parameters.Add(new SqlParameter("@FechaPerdida", SqlDbType.VarChar));
            parameters[2].Value = t.FechaPerdida;
            parameters.Add(new SqlParameter("@LugarPerdida", SqlDbType.VarChar));
            parameters[3].Value = t.LugarPerdida;
            parameters.Add(new SqlParameter("@Detalles", SqlDbType.VarChar));
            parameters[4].Value = t.Detalles;
            parameters.Add(new SqlParameter("@Referencia", SqlDbType.VarChar));
            parameters[5].Value = t.Referencia;
            parameters.Add(new SqlParameter("@Pais", SqlDbType.VarChar));
            parameters[6].Value = t.Pais;
            parameters.Add(new SqlParameter("@Region", SqlDbType.VarChar));
            parameters[7].Value = t.Region;
            parameters.Add(new SqlParameter("@Ciudad", SqlDbType.VarChar));
            parameters[8].Value = t.Ciudad;

            var idBusqueda = base.Insert("uspPersonaAdd", parameters.ToArray());

            return persona;
        }
    }
}
