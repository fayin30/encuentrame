﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Encuentrame.Business.Entity;

namespace Encuentrame.Business.Data
{
  public class HistorialDA : BaseData, IData<Historial>
  {
    public bool Actualizar(Historial t)
    {
      throw new NotImplementedException();
    }

    public bool Eliminar(Historial t)
    {
      throw new NotImplementedException();
    }

    public Historial ObtenerPorId(int id)
    {
      throw new NotImplementedException();
    }

    public Historial Registrar(Historial t)
    {
      Historial historial = null;
      var parameters = new List<SqlParameter>();
      var par = new SqlParameter("@IdPersonaAlerta", SqlDbType.Int);
      par.Value = t.PersonaAlerta.IdPersona;
      parameters.Add(par);
      
      base.Insert("uspHistorialAdd", parameters.ToArray());

      return historial;
    }
  }
}
