﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Encuentrame.Business.Entity;
using System.Data.SqlClient;
using System.Data;

namespace Encuentrame.Business.Data
{
    public class PersonaDA : BaseData, IData<Persona>
    {
        public bool Actualizar(Persona t)
        {
            throw new NotImplementedException();
        }

        public bool Eliminar(Persona t)
        {
            throw new NotImplementedException();
        }

        public Persona ObtenerPorId(int id)
        {
            Persona persona = null;
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@IdPersona", SqlDbType.Int));
            parameters[0].Value = id;
            using (var dr = base.GetDataReader("uspPersonaGetXId", parameters.ToArray()))
            {
                while (dr.Read())
                {
                    persona = new Persona()
                    {
                        IdPersona = dr.GetInt32(dr.GetOrdinal("IdPersona")),
                        Apellidos = dr.GetString(dr.GetOrdinal("Apellidos")),
                        Comentario = dr.GetString(dr.GetOrdinal("Comentario")),
                        Direccion = dr.GetString(dr.GetOrdinal("Direccion")),
                        DNI = dr.GetString(dr.GetOrdinal("DNI")),
                        FechaRegistro = DateTime.Now,
                        Nombres = dr.GetString(dr.GetOrdinal("Nombres")),
                        Telefono = dr.GetString(dr.GetOrdinal("Telefono")),
                    };
                }
            }
            return persona;
        }

        public Persona Registrar(Persona t)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@IdPersona", SqlDbType.Int));
            parameters[0].Direction = ParameterDirection.Output;
            parameters.Add(new SqlParameter("@Nombres", SqlDbType.VarChar));
            parameters[1].Value = t.Nombres;
            parameters.Add(new SqlParameter("@Apellidos", SqlDbType.VarChar));
            parameters[2].Value = t.Apellidos;
            parameters.Add(new SqlParameter("@Telefono", SqlDbType.VarChar));
            parameters[3].Value = t.Telefono;
            parameters.Add(new SqlParameter("@Direccion", SqlDbType.VarChar));
            parameters[4].Value = t.Direccion;
            parameters.Add(new SqlParameter("@Comentario", SqlDbType.VarChar));
            parameters[5].Value = t.Comentario;
            parameters.Add(new SqlParameter("@DNI", SqlDbType.VarChar));
            parameters[6].Value = t.DNI;
            parameters.Add(new SqlParameter("@Tipo", SqlDbType.VarChar));
            parameters[7].Value = t.TipoPersona;
            parameters.Add(new SqlParameter("@Email", SqlDbType.VarChar));
            parameters[8].Value = t.Login;

            var idPersona = base.Insert("uspPersonaAdd", parameters.ToArray(), "@IdPersona");
            t.IdPersona = idPersona;

            return t;
        }
    }
}
