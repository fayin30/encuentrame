﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encuentrame.Business.Entity
{
    public class Archivo : BaseEntity
    {
        public int IdArchivo { get; set; }
        public int IdPersona { get; set; }
        public string RutaArchivo { get; set; }
        public Byte[] Contenido { get; set; }        
    }
}
