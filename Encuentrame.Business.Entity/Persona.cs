﻿using System;
using System.Collections.Generic;

namespace Encuentrame.Business.Entity
{
    public class Persona : BaseEntity
    {
        public int IdPersona { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string DNI { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Comentario { get; set; }
        public string TipoPersona { get; set; }
        public string Login { get; set; }
    public string Password { get; set; }
    public List<Archivo> LstArchivo { get; set; }

        
    }
}
