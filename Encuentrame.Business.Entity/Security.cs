﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encuentrame.Business.Entity
{
    public class Security
    {
        public int IdSecurity { get; set; }
        public string Token { get; set; }
        public int IdUser { get; set; }
        public Persona Persona { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
