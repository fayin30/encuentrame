﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encuentrame.Business.Entity
{
    public class Busqueda
    {
        public int IdBusqueda { get; set; }
        public Persona PersonaResponsable { get; set; }
        public Persona PersonaPerdida { get; set; }
        public DateTime FechaPerdida { get; set; }
        public string LugarPerdida { get; set; }
        public string Detalles { get; set; }
        public string Referencia { get; set; }
        public string Pais { get; set; }
        public string Region { get; set; }
        public string Ciudad { get; set; }
        public bool Encontrado { get; set; }
        public DateTime FechaEncuentro { get; set; }
    }
}
