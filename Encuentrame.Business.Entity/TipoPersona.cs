﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encuentrame.Business.Entity
{
    public class TipoPersona
    {
        public int IdTipoPersona { get; set; }
        public string Nombre { get; set; }
        public string Codigo { get; set; }
    }
}
