﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encuentrame.Business.Entity
{
    public class Historial
    {
        public int IdHistorial { get; set; }
        public Persona PersonaAlerta { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}
