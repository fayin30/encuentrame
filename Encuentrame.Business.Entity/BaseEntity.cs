﻿using System; 
namespace Encuentrame.Business.Entity
{
    public class BaseEntity
    {
        public int Id { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}
