﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Encuentrame.Business.Data;
using Encuentrame.Business.Entity;

namespace Encuentrame.Business.Logic
{
    public class PersonaBL : BaseBusinessLogic<Persona>, IBusinessLogic<Persona>
    {
        public PersonaBL() : base(new PersonaDA())
        { 
        }

        public List<Persona> ObtenerPersona(Persona filtro)
        {
            List<Persona> _retorno = new List<Persona>();

            _retorno.Add(new Persona() {
                IdPersona = 1,
                Nombres = "demo 1",
                Apellidos = "demo 1 1"
            });
            _retorno.Add(new Persona()
            {
                IdPersona = 1,
                Nombres = "demo2",
                Apellidos = "demo 2 2"
            });
            return _retorno;
        }
    }
}
