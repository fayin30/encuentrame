﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Encuentrame.Business.Entity;

namespace Encuentrame.Business.Logic
{
  public interface IBusinessLogic<T>
  {
    T Registrar(T t);
    bool Actualizar(T t);
    bool Eliminar(T t);
    T ObtenerPorId(int id);
  }
}
