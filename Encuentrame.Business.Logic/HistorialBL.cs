﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Encuentrame.Business.Data;
using Encuentrame.Business.Entity;

namespace Encuentrame.Business.Logic
{
  public class HistorialBL : BaseBusinessLogic<Historial>, IBusinessLogic<Historial>
  {
    public HistorialBL()
      : base(new HistorialDA())
    { }
  }
}
