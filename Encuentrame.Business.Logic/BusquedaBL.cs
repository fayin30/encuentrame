﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Encuentrame.Business.Data;
using Encuentrame.Business.Entity;

namespace Encuentrame.Business.Logic
{
  public class BusquedaBL : BaseBusinessLogic<Busqueda>, IBusinessLogic<Busqueda>
  {
    public BusquedaBL()
      : base(new BusquedaDA()) { }
  }
}
