﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Encuentrame.Business.Data;

namespace Encuentrame.Business.Logic
{
  public abstract class BaseBusinessLogic<T> where T : class 
  {
    private IData<T> _idata;

    public BaseBusinessLogic(IData<T> idata)
    {
      _idata = idata;
    }

    public T Registrar(T t)
    {
      return _idata.Registrar(t);
    }

    public bool Actualizar(T t)
    {
      return _idata.Actualizar(t);
    }

    public bool Eliminar(T t)
    {
      return _idata.Eliminar(t);
    }

    public T ObtenerPorId(int id)
    {
      return _idata.ObtenerPorId(id);
    }
  }
}
