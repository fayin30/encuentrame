﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EncuentrameWebApi.Controllers.Models
{
    public class AlertaDTO
    {
        public int CodigoPersonaAlerta { get; set; }
        public int CodigoBusqueda { get; set; }
        public DateTime FechaAlerta { get; set; }
        public string Referencia { get; set; }
        public string Detalle { get; set; }
        public string Direccion { get; set; }
        public string Pais { get; set; }
        public string Region { get; set; }
        public string Ciudad { get; set; }
        public bool Exito { get; set; }
        public DateTime FechaExito { get; set; }
    }
}