﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EncuentrameWebApi.Controllers.Models
{
    public class ArchivoDTO
    {
        public int IdPersona { get; set; }
        public string RutaArchivo { get; set; }
    }
}