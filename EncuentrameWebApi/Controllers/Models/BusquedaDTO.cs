﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EncuentrameWebApi.Controllers.Models
{
    public class BusquedaDTO
    {  
        public string NombreResponsable { get; set; }
        public string ApellidoResponsable { get; set; }
        public string DNIResponsable { get; set; }
        public string DireccionResponsable { get; set; }
        public string TelefonoResponsable { get; set; }
        
        public string NombrePerdida { get; set; }
        public string ApellidoPerdida { get; set; }
        public string DNIPerdida { get; set; }
        public string DireccionPerdida { get; set; }
        public string TelefonoPerdida { get; set; }

        public DateTime FechaPerdida { get; set; }
        public string LugarPerdida { get; set; }
        public string Detalles { get; set; }
        public string Referencia { get; set; }
        public string Pais { get; set; }
        public string Region { get; set; }
        public string Ciudad { get; set; }
        public bool Encontrado { get; set; }
        public DateTime FechaEncuentro { get; set; }
    }
}