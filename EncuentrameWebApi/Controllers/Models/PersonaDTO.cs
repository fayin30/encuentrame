﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EncuentrameWebApi.Controllers.Models
{
    public class PersonaDTO
    {
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string DNI { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Comentario { get; set; }
        public string TipoPersona { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}