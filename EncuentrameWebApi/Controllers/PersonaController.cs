﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Encuentrame.Business.Logic;
using Encuentrame.Business.Entity;
using System.Threading.Tasks;
using System.Web;
using EncuentrameWebApi.Controllers.Models;
using EncuentrameWebApi.Controllers.Mapping;

namespace EncuentrameWebApi.Controllers
{
  public class PersonaController : ApiController
  {
    [Route("getsecurity")]
    public IHttpActionResult GetUser(string user, string pws)
    {
      //Security _response = new Security();
      //PersonaBL _negocio = new PersonaBL();
      //_response = _negocio.ObtenerPorId(1);

      //if (_response == null)
      //{
      //    return NotFound();
      //}
      return Ok();
    }

    //--------Alerta POST: api/Persona
    [Route("ConsultaPersonaId")]
    public IHttpActionResult GetPersona(int id)
    {
      Persona _response = new Persona();
      PersonaBL _negocio = new PersonaBL();
      _response = _negocio.ObtenerPorId(id);

      if (_response == null)
      {
        return NotFound();
      }
      return Ok(_response);
    }

    [Route("ConsultaPersonaAll")]
    public IHttpActionResult GetPersonaAll(string nombre = null, string dni = null)
    {
      List<Persona> _response = new List<Persona>();
      Persona _filtro = new Persona();
      PersonaBL _negocio = new PersonaBL();

      _filtro.Nombres = nombre;
      _filtro.DNI = dni;
      _response = _negocio.ObtenerPersona(_filtro);

      if (_response == null)
      {
        return NotFound();
      }
      return Ok(_response);
    }

    [Route("RegisterPeople")]
    public void registrar_persona([FromBody]Persona _persona)
    {
      PersonaBL _negocio = new PersonaBL();
      BusquedaBL _negociobusqueda = new BusquedaBL();
      ArchivoBL _negocioFile = new ArchivoBL();
      UsuarioBL _userBL = new UsuarioBL();

      Persona _persona_response = new Persona();
      Busqueda _busqueda = new Busqueda();
      Security _securyti = new Security();

      _persona_response = _negocio.Registrar(_persona);
      _securyti.Persona = new Persona() { Id = _persona_response.IdPersona };
      _securyti.Login = _persona.Login;
      _securyti.Password = _persona.Password;
      _securyti = _userBL.Registrar(_securyti);
      if (_persona.LstArchivo != null)
        foreach (var iFile in _persona.LstArchivo)
        {
          iFile.IdPersona = _persona_response.IdPersona;
          _negocioFile.Registrar(iFile);
        }

    } 

        [Route("ModificaPeople")]
        public void modificar_persona([FromBody]Persona _persona)
        {
            PersonaBL _negocio = new PersonaBL();
            _negocio.Actualizar(_persona);
        }

        //--------File
        [Route("ConsultaFotos")]
        public IHttpActionResult GetPersonaFoto(int id)
        {
            Archivo _response = new Archivo();
            ArchivoBL _negocio = new ArchivoBL();

            _response = _negocio.ObtenerPorId(id);

            if (_response == null)
            {
                return NotFound();
            }
            return Ok(_response);
        }

        [HttpPost, Route("RegisterFileFisico")]
        public string MyFileUpload()
        {
            var request = HttpContext.Current.Request;
            var filePath = " F:\\Proyectos\\Fayin3.0\\EncuentrameWebApi\\" + request.Headers["filename"];
            using (var fs = new System.IO.FileStream(filePath, System.IO.FileMode.Create))
            {
                request.InputStream.CopyTo(fs);
            }
            return "uploaded";
        }

        [Route("RegisterFile")]
        public void registrar_achivo([FromBody]List<Archivo> _archivos) {
            ArchivoBL _negocio = new ArchivoBL();
            foreach (var iFile in _archivos)
            {
                _negocio.Registrar(iFile);
            }            
        }

        //--------Alerta
        [Route("ConsultaAlerta")]
        public IHttpActionResult GetAlerta(int id)
        {
            Alerta _response = new Alerta();
            AlertaBL _negocio = new AlertaBL();
            _response = _negocio.ObtenerPorId(id);

            if (_response == null)
            {
                return NotFound();
            }
            return Ok(_response);
        }

        [Route("RegisterAlerta")]
        public void registrar_alerta([FromBody]AlertaDTO _request)
        {
            AlertaBL _negocio = new AlertaBL();
            Alerta _alerta = new Alerta();
            _alerta = MappingDTO.MappingAlert(_request);
            _negocio.Registrar(_alerta);
        }

        [Route("ModificarAlerta")]
        public void modificar_alerta([FromBody]Alerta _alerta)
        {
            AlertaBL _negocio = new AlertaBL();
            _negocio.Actualizar(_alerta);
        }

        //--------Busqueda
        [Route("ConsultaBusqueda")]
        public IHttpActionResult GetBusquedaAll()
        {
            List<Busqueda> _response = new List<Busqueda>();
            Busqueda _filtro = new Busqueda();
            BusquedaBL _negocio = new BusquedaBL();

            _filtro = _negocio.ObtenerPorId(1);

            if (_response == null)
            {
                return NotFound();
            }
            return Ok(_response);
        }

        [Route("RegistoBusqueda")]
        public void registrar_busqueda([FromBody]BusquedaDTO _request)
        {
            BusquedaBL _negociobusqueda = new BusquedaBL();
            Busqueda _respBusqueda = new Busqueda();
            Busqueda _responseBusqueda = new Busqueda();
            Persona _responsePersonaResponsable = new Persona();
            Persona _responsePersonaPerdida = new Persona();
            PersonaBL _negocioPersona = new PersonaBL();

            _respBusqueda = MappingDTO.MappingBusqueda(_request);
            _respBusqueda = _negociobusqueda.Registrar(_respBusqueda);
            _responsePersonaResponsable = _negocioPersona.Registrar(_respBusqueda.PersonaResponsable);
            _responsePersonaPerdida = _negocioPersona.Registrar(_respBusqueda.PersonaPerdida);
        }
    }
}
