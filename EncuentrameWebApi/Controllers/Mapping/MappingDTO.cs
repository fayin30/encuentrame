﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Encuentrame.Business.Entity;
using EncuentrameWebApi.Controllers.Models;

namespace EncuentrameWebApi.Controllers.Mapping
{
    public class MappingDTO
    {
        public static Busqueda MappingBusqueda(BusquedaDTO _request) {
            Busqueda _response = new Busqueda();
            _response.IdBusqueda = 0; 
            _response.FechaPerdida = _request.FechaPerdida;
            _response.LugarPerdida = _request.LugarPerdida;
            _response.Detalles = _request.Detalles;
            _response.Referencia = _request.Referencia;
            _response.Pais = _request.Pais;
            _response.Region = _request.Region;
            _response.Ciudad = _request.Ciudad;
            _response.Encontrado = _request.Encontrado;
            _response.FechaEncuentro = _request.FechaEncuentro;

            _response.PersonaResponsable = new Persona();
            _response.PersonaResponsable.Nombres = _request.NombreResponsable;
            _response.PersonaResponsable.Apellidos = _request.ApellidoResponsable;
            _response.PersonaResponsable.DNI= _request.DNIResponsable;
            _response.PersonaResponsable.Direccion = _request.DireccionResponsable;
            _response.PersonaResponsable.Telefono= _request.TelefonoResponsable;

            _response.PersonaPerdida = new Persona();
            _response.PersonaPerdida.Nombres = _request.NombreResponsable;
            _response.PersonaPerdida.Apellidos = _request.ApellidoResponsable;
            _response.PersonaPerdida.DNI = _request.DNIResponsable;
            _response.PersonaPerdida.Direccion = _request.DireccionResponsable;
            _response.PersonaPerdida.Telefono = _request.TelefonoResponsable;

            return _response;
        }

        public static Alerta MappingAlert(AlertaDTO _request) {
            Alerta response = new Alerta();
            response.PersonaAlerta = new Persona();
            response.Busqueda = new Busqueda();
            response.PersonaAlerta.IdPersona = _request.CodigoPersonaAlerta;
            response.Busqueda.IdBusqueda = _request.CodigoBusqueda;
            response.FechaAlerta = _request.FechaAlerta;
            response.Referencia = _request.Referencia;
            response.Detalle = _request.Detalle;
            response.Direccion = _request.Direccion;
            response.Pais = _request.Pais;
            response.Region = _request.Region;
            response.Ciudad = _request.Ciudad;
            response.Exito = _request.Exito;
            response.FechaExito = _request.FechaExito;
            return response;
        }
    }
}